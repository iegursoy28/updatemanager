
using System;
using System.Collections.Generic;

public class DBManager
{
    private static readonly Logger Log = new Logger(typeof(DBManager).Name) { IsLog = true };

    private static readonly string _DB_FILE_NAME = ".db.json";

    private static DBManager db { get; set; }
    public Dictionary<string, string> Map { get; private set; }

    public static DBManager NewInstance()
    {
        if (db != null)
            return db;

        try
        {
            string j = System.IO.File.ReadAllText(_DB_FILE_NAME);
            if (j == null || j.Length <= 0)
                throw new Exception("DB file is empty!");

            db = new DBManager();
            db.Map = System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, string>>(j);
            Log.Info("DB create from '.db.json' file.");
        }
        catch (Exception e)
        {
            Log.Info("DB create from file error: " + e);
            db = new DBManager();
            db.Map = new Dictionary<string, string>();
            System.IO.File.WriteAllText(_DB_FILE_NAME, System.Text.Json.JsonSerializer.Serialize(db.Map));
            Log.Info("DB create new instance.");
        }

        return db;
    }

    public void Put(string key, string obj)
    {
        try
        {
            if (key == null || key.Length <= 0)
                throw new Exception("Key can not empty!");
            if (obj == null)
                throw new Exception("Object can not null!!");

            if (Map.ContainsKey(key))
                Map[key] = obj;
            else
                Map.Add(key, obj);
            System.IO.File.WriteAllText(_DB_FILE_NAME, System.Text.Json.JsonSerializer.Serialize(db.Map));
        }
        catch (Exception e)
        {
            Log.Info("DB Put Error: " + e.Message);
        }
    }
}
