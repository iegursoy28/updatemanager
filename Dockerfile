FROM mcr.microsoft.com/dotnet/core/sdk:3.0

COPY . /server/

ENTRYPOINT [ "dotnet", "run", "-c", "Release", "-r", "linux-x64", "-p", "/server" ]

EXPOSE 5028
