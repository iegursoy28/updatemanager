using System;

public class TaskModel
{
    public enum InstallationType { TAR, EXECUTABLE, ZIP, BASH }

    public long Id { get; private set; }
    public string Name { get; set; }
    public string WorkDir { get; set; }
    public string FullFilePath { get; set; }
    public InstallationType Type { get; set; }
    public TaskCMDModel[] PreInstallationCmds { get; set; }
    public TaskCMDModel[] InstallationCmds { get; set; }
    public TaskCMDModel[] PostInstallationCmds { get; set; }

    public TaskModel()
    {
        Id = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
    }

    override public string ToString()
    {
        return System.Text.Json.JsonSerializer.Serialize(this);
    }
}
