using System;

public class TaskCMDModel
{
    public long Id { get; private set; }
    public string Cmd { get; set; }
    public bool IsAfterReboot { get; set; }

    public TaskCMDModel()
    {
        Id = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
    }

    override public string ToString()
    {
        return System.Text.Json.JsonSerializer.Serialize(this);
    }
}