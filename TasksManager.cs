using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class TasksManager
{
    private static readonly Logger Log = new Logger(typeof(TasksManager).Name) { IsLog = true };

    private static TasksManager manager { get; set; }

    public TaskModel TaskQ { get; private set; }
    public TaskStatus TaskStatusQ { get; private set; }

    public static TasksManager NewInstance()
    {
        if (manager == null)
            manager = new TasksManager();

        if (DBManager.NewInstance().Map.ContainsKey("task"))
        {
            var j = DBManager.NewInstance().Map.GetValueOrDefault("task", "");
            if (j.Length <= 0)
                return manager;

            var t = System.Text.Json.JsonSerializer.Deserialize<TaskModel>(j);
            if (t == null)
                return manager;

            manager.TaskQ = t;
        }

        return manager;
    }

    public TasksManager()
    {
        TaskQ = new TaskModel();
        TaskStatusQ = new TaskStatus();
    }

    public void PutTask(TaskModel task)
    {
        if (task == null)
            throw new Exception("Task can not null!!");
        if (TaskStatusQ.IsRunning)
            throw new Exception($"There is already a task!!\n{TaskQ}");

        TaskQ = task;
        TaskStatusQ.StatusClear();

        if (TaskQ.PreInstallationCmds != null && TaskQ.PreInstallationCmds.Length > 0) TaskStatusQ.StageCount += TaskQ.PreInstallationCmds.Length;
        if (TaskQ.InstallationCmds != null && TaskQ.InstallationCmds.Length > 0) TaskStatusQ.StageCount += TaskQ.InstallationCmds.Length;
        if (TaskQ.PostInstallationCmds != null && TaskQ.PostInstallationCmds.Length > 0) TaskStatusQ.StageCount += TaskQ.PostInstallationCmds.Length;

        DBManager.NewInstance().Put("task", TaskQ.ToString());
        Log.Info($"Set task: {TaskQ}");
    }

    public void PutDefaultInstallationTask(TaskModel.InstallationType type, string path)
    {
        var t = Utils.GetInstallationModel(DBManager.NewInstance(), type, path);
        PutTask(t);
    }

    public void RunTaskWithType(TaskModel.InstallationType type)
    {
        PutDefaultInstallationTask(type, null);
        RunTask();
    }

    public void RunTaskWithType(TaskModel.InstallationType type, string path)
    {
        PutDefaultInstallationTask(type, path);
        RunTask();
    }

    public void RunTask()
    {
        new Task(() =>
        {
            string task_log = "";
            try
            {
                if (TaskQ == null)
                    throw new Exception("Not found Task!") { };
                if (TaskStatusQ.IsRunning)
                    throw new Exception("Already a task is running!");
                if (TaskStatusQ.StageCount <= 0)
                    throw new Exception("Not found cmd for installation!");

                TaskStatusQ.IsRunning = true;
                TaskStatusQ.StageProgress = 0;
                TaskStatusQ.StageSubProgress = -1;

                int tmpP;
                string tmpMsg;

                if (TaskQ.PreInstallationCmds != null && TaskQ.PreInstallationCmds.Length > 0)
                {
                    TaskStatusQ.RunningState = TaskStatus.RunningCMDType.BEFORE;
                    for (int i = 0; i < TaskQ.PreInstallationCmds.Length; i++)
                    {
                        TaskStatusQ.StageProgress++;
                        TaskStatusQ.StageCMD = TaskQ.PreInstallationCmds[i].Cmd;

                        string cmd_log = Utils.RunCMD(TaskQ.WorkDir, TaskStatusQ.StageCMD, (l) =>
                        {
                            string log = $"cmd({TaskStatusQ.StageProgress}/{TaskStatusQ.StageCount}):{TaskStatusQ.StageCMD} => {l}";
                            task_log += log;
                            if (Utils.ParseSubProgress(l, out tmpP, out tmpMsg))
                            {
                                TaskStatusQ.StageSubProgress = tmpP;
                                TaskStatusQ.StageSubProgressMsg = tmpMsg;
                            }
                            Log.Debug(log);
                        });
                    }
                }

                if (TaskQ.InstallationCmds != null && TaskQ.InstallationCmds.Length > 0)
                {
                    TaskStatusQ.RunningState = TaskStatus.RunningCMDType.CURRENT;
                    for (int i = 0; i < TaskQ.InstallationCmds.Length; i++)
                    {
                        TaskStatusQ.StageProgress++;
                        TaskStatusQ.StageCMD = TaskQ.InstallationCmds[i].Cmd;

                        string cmd_log = Utils.RunCMD(TaskQ.WorkDir, TaskStatusQ.StageCMD, (l) =>
                        {
                            string log = $"cmd({TaskStatusQ.StageProgress}/{TaskStatusQ.StageCount}):{TaskStatusQ.StageCMD} => {l}";
                            task_log += log;
                            if (Utils.ParseSubProgress(l, out tmpP, out tmpMsg))
                            {
                                TaskStatusQ.StageSubProgress = tmpP;
                                TaskStatusQ.StageSubProgressMsg = tmpMsg;
                            }
                            Log.Debug(log);
                        });
                    }
                }

                if (TaskQ.PostInstallationCmds != null && TaskQ.PostInstallationCmds.Length > 0)
                {
                    TaskStatusQ.RunningState = TaskStatus.RunningCMDType.AFTER;
                    for (int i = 0; i < TaskQ.PostInstallationCmds.Length; i++)
                    {
                        TaskStatusQ.StageProgress++;
                        TaskStatusQ.StageCMD = TaskQ.PostInstallationCmds[i].Cmd;

                        string cmd_log = Utils.RunCMD(TaskQ.WorkDir, TaskStatusQ.StageCMD, (l) =>
                        {
                            string log = $"cmd({TaskStatusQ.StageProgress}/{TaskStatusQ.StageCount}):{TaskStatusQ.StageCMD} => {l}";
                            task_log += log;
                            if (Utils.ParseSubProgress(l, out tmpP, out tmpMsg))
                            {
                                TaskStatusQ.StageSubProgress = tmpP;
                                TaskStatusQ.StageSubProgressMsg = tmpMsg;
                            }
                            Log.Debug(log);
                        });
                    }
                }

                //Log.Debug(task_log);
            }
            catch (Exception e)
            {
                TaskStatusQ.StatusSetError(1, e.Message);
            }

            TaskStatusQ.StatusSuccessfullyFinish();
        }).Start();
    }

    public string GetStatus()
    {
        if (TaskQ == null)
        {
            TaskStatusQ.ErrorCode = 1;
            TaskStatusQ.ErrorMsg = "Not found task!";
        }

        return TaskStatusQ.ToString();
    }
}