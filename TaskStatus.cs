using System;

public class TaskStatus
{
    public enum RunningCMDType { NONE, BEFORE, CURRENT, AFTER }

    public bool IsRunning { get; set; }
    public RunningCMDType RunningState { get; set; }
    public string StageCMD { get; set; }
    public int StageProgress { get; set; }
    public int StageSubProgress { get; set; }
    public string StageSubProgressMsg { get; set; }
    public int StageCount { get; set; }

    public int ErrorCode { get; set; }
    public string ErrorMsg { get; set; }

    public void StatusClear()
    {
        this.IsRunning = false;
        this.RunningState = RunningCMDType.NONE;
        this.StageCMD = "";
        this.StageProgress = 0;
        this.StageSubProgress = -1;
        this.StageSubProgressMsg = "";
        this.StageCount = 0;

        this.ErrorCode = 0;
        this.ErrorMsg = "";
    }

    public void StatusSuccessfullyFinish()
    {
        this.IsRunning = false;
        this.RunningState = RunningCMDType.NONE;
        this.StageCMD = "";

        this.ErrorCode = 0;
        this.ErrorMsg = "Task successfully finish";
    }

    public void StatusSetError(int code, string msg)
    {
        this.IsRunning = false;
        this.RunningState = RunningCMDType.NONE;
        this.StageCMD = "";

        this.ErrorCode = code;
        this.ErrorMsg = $"{msg}";
    }

    override public string ToString()
    {
        if (ErrorCode > 0)
            return string.Format("{{\"error\":{{\"code\":{0},\"message\":\"{1}\"}}}}", ErrorCode, ErrorMsg);

        return System.Text.Json.JsonSerializer.Serialize(this);
    }
}