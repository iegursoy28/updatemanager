using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace UpdateManager.Controllers
{
    [ApiController]
    [Route("tasks")]
    [EnableCors("AllowOrigin")]
    public class TasksController : ControllerBase
    {
        private static readonly Logger Log = new Logger(typeof(TasksController).Name) { IsLog = true };

        [HttpGet]
        public IEnumerable<TaskModel> Get()
        {
            Log.Debug($"GET: {TasksManager.NewInstance().TaskQ}");
            return new List<TaskModel>() { TasksManager.NewInstance().TaskQ };
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody]TaskModel task)
        {
            Log.Debug($"CREATE: {task}");
            try
            {
                TasksManager.NewInstance().PutTask(task);
                return Ok($"{TasksManager.NewInstance().TaskQ}");
            }
            catch (Exception e)
            {
                Log.Error($"CREATE: {e.Message}", e);
                return BadRequest(e);
            }
        }

        [HttpGet]
        [Route("run")]
        public IActionResult Run()
        {
            Log.Debug($"RUN: {TasksManager.NewInstance().TaskStatusQ}");
            if (TasksManager.NewInstance().TaskStatusQ.IsRunning)
                return Ok(TasksManager.NewInstance().TaskStatusQ);

            TasksManager.NewInstance().RunTask();
            return Ok($"{TasksManager.NewInstance().TaskStatusQ}");
        }

        [HttpGet]
        [Route("run/{id}")]
        public IActionResult RunWithType([FromRoute]int id)
        {
            Log.Debug($"RunWithType:[{id}] {TasksManager.NewInstance().TaskQ}");

            if (TasksManager.NewInstance().TaskStatusQ.IsRunning)
                return Ok(TasksManager.NewInstance().TaskStatusQ);

            try
            {
                TaskModel.InstallationType t = (TaskModel.InstallationType)Enum.ToObject(typeof(TaskModel.InstallationType), id);
                TasksManager.NewInstance().RunTaskWithType(t);
                return Ok($"{TasksManager.NewInstance().TaskStatusQ}");
            }
            catch (Exception e)
            {
                Log.Error($"Unknown type {id}", e);
                return BadRequest($"Unknown type {id}");
            }
        }

        [HttpGet]
        [Route("run/{id}/{path}")]
        public IActionResult RunWithTypeAndPath([FromRoute]int id, [FromRoute]string path)
        {
            string base64Decoded;
            try
            {
                byte[] data = System.Convert.FromBase64String(path);
                base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data);
            }
            catch (Exception e)
            {
                Log.Error($"Can not decode {path}", e);
                return BadRequest($"Can not decode  {path}");
            }

            Log.Debug($"RunWithTypeAndPath:[{id}, {base64Decoded}] {TasksManager.NewInstance().TaskQ}");

            if (TasksManager.NewInstance().TaskStatusQ.IsRunning)
                return Ok(TasksManager.NewInstance().TaskStatusQ);

            try
            {
                TaskModel.InstallationType t = (TaskModel.InstallationType)Enum.ToObject(typeof(TaskModel.InstallationType), id);
                TasksManager.NewInstance().RunTaskWithType(t, base64Decoded);
                return Ok($"{TasksManager.NewInstance().TaskStatusQ}");
            }
            catch (Exception e)
            {
                Log.Error($"Unknown type {id}", e);
                return BadRequest($"Unknown type {id}");
            }
        }

        [HttpGet]
        [Route("status")]
        public IActionResult Status()
        {
            Log.Debug($"STATUS: {TasksManager.NewInstance().GetStatus()}");
            return Ok($"{TasksManager.NewInstance().GetStatus()}");
        }

        [HttpPost]
        [Route("set_template")]
        public IActionResult SetTemplate([FromBody]TaskModel task)
        {
            Log.Debug($"SetTemplate: {task}");
            try
            {
                DBManager db = DBManager.NewInstance();
                Utils.SetInstallationModel(db, task);
                return Ok($"{task}");
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet]
        [Route("installation_templates")]
        public IEnumerable<TaskModel> GetTemplates()
        {
            DBManager db = DBManager.NewInstance();
            var l = new List<TaskModel>(){
                Utils.GetInstallationModel(db, TaskModel.InstallationType.TAR, null),
                Utils.GetInstallationModel(db, TaskModel.InstallationType.EXECUTABLE, null),
                Utils.GetInstallationModel(db, TaskModel.InstallationType.ZIP, null),
                Utils.GetInstallationModel(db, TaskModel.InstallationType.BASH, null)
            };
            Log.Debug($"GetTemplates: {l.ToString()}");

            return l;
        }
    }
}
