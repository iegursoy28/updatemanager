using System;

public class UpdateManagerException : System.Exception
{
    public int ErrorCode { get; set; } = 0;

    public UpdateManagerException() : base()
    { }

    public UpdateManagerException(int code, string message) : base(message)
    {
        ErrorCode = code;
    }

    public UpdateManagerException(int code, string message, Exception inner) : base(message, inner)
    {
        ErrorCode = code;
    }
}