# Update Manager Service

This application can run the given work list on the console while it is running on the system.

## User Manual

When defining a job, select the type according to the following enum values.

```c#
public enum InstallationType { TAR, EXECUTABLE, ZIP, BASH }
```

### List Tasks

To pull the defined worklist, you must create a request as follows.

```sh
curl --request GET \
--url http://0.0.0.0:5028/tasks/installation_templates
```

<details>
<summary>Sample Response <strong>List Tasks</strong></summary>

```json
[
  {
    "id": 1573133292154,
    "name": "installationtype_tar",
    "workDir": "/tmp/",
    "type": 0,
    "preInstallationCmds": [
      {
        "id": 1573133292154,
        "cmd": "mkdir -p /setup",
        "isAfterReboot": false
      },
      {
        "id": 1573133292154,
        "cmd": "tar -xf installation.tar.gz -C /setup",
        "isAfterReboot": false
      }
    ],
    "installationCmds": [
      {
        "id": 1573133292154,
        "cmd": "cd /setup && chmod +x install.sh && ./install.sh",
        "isAfterReboot": false
      }
    ],
    "postInstallationCmds": null
  },
  {
    "id": 1573133292160,
    "name": "installationtype_executable",
    "workDir": "/",
    "type": 1,
    "preInstallationCmds": null,
    "installationCmds": null,
    "postInstallationCmds": null
  },
  {
    "id": 1573133292160,
    "name": "installationtype_zip",
    "workDir": "/",
    "type": 2,
    "preInstallationCmds": null,
    "installationCmds": null,
    "postInstallationCmds": null
  },
  {
    "id": 1573133292160,
    "name": "installationtype_bash",
    "workDir": "/tmp/",
    "type": 3,
    "preInstallationCmds": [
      {
        "id": 1573133292160,
        "cmd": "ls -la",
        "isAfterReboot": false
      },
      {
        "id": 1573133292160,
        "cmd": "df -h",
        "isAfterReboot": false
      }
    ],
    "installationCmds": [
      {
        "id": 1573133292160,
        "cmd": "for i in {1..20}; do echo ieg; sleep 0.5; done",
        "isAfterReboot": false
      },
      {
        "id": 1573715281325,
        "cmd": "for i in {1..30}; do echo !==ieg==!; sleep 0.5; done",
        "isAfterReboot": false
      },
      {
        "id": 1573715281325,
        "cmd": "for i in {1..100}; do echo !==\"$i\"==!; sleep 0.5; done",
        "isAfterReboot": false
      },
      {
        "id": 1573715281325,
        "cmd": "for i in {1..100}; do echo !==\"$i  : ieg\"==!; sleep 0.5; done",
        "isAfterReboot": false
      }
    ],
    "postInstallationCmds": []
  }
]
```

</details>

### Set Task

```sh
curl --request POST \
  --url http://0.0.0.0:5028/tasks/set_template \
  --header 'content-type: application/json' \
  --data '{
    "name": "my_custom_task",
    "workDir": "/tmp/",
    "type": 3,
    "preInstallationCmds": [
        {
            "cmd": "ls -la",
            "isAfterReboot": false
        },
        {
            "cmd": "df -h",
            "isAfterReboot": false
        }
    ],
    "installationCmds": [
        {
            "cmd": "for i in {1..20}; do echo ieg; sleep 0.5; done",
            "isAfterReboot": false
        },
        {
            "cmd": "for i in {1..30}; do echo !==ieg==!; sleep 0.5; done",
            "isAfterReboot": false
        },
        {
            "cmd": "for i in {1..100}; do echo !==\"$i\"==!; sleep 0.5; done",
            "isAfterReboot": false
        },
        {
            "cmd": "for i in {1..100}; do echo !==\"$i  : ieg\"==!; sleep 0.5; done",
            "isAfterReboot": false
        }
    ],
    "postInstallationCmds": [
    ]
    }'
```

<details>
<summary>Sample Response <strong>Set Task</strong></summary>

```json
{
  "Id": 1573133265883,
  "Name": "installationtype_bash",
  "WorkDir": "/tmp/",
  "Type": 3,
  "PreInstallationCmds": [
    {
      "Id": 1573133265889,
      "Cmd": "ls -la",
      "IsAfterReboot": false
    },
    {
      "Id": 1573133265890,
      "Cmd": "df -h",
      "IsAfterReboot": false
    }
  ],
  "InstallationCmds": [
    {
      "Id": 1573133265891,
      "Cmd": "for i in {1..20}; do echo ieg; sleep 0.5; done",
      "IsAfterReboot": false
    },
    {
      "Id": 1573715348324,
      "Cmd": "for i in {1..30}; do echo !==ieg==!; sleep 0.5; done",
      "IsAfterReboot": false
    },
    {
      "Id": 1573715348324,
      "Cmd": "for i in {1..100}; do echo !==\"$i\"==!; sleep 0.5; done",
      "IsAfterReboot": false
    },
    {
      "Id": 1573715348324,
      "Cmd": "for i in {1..100}; do echo !==\"$i  : ieg\"==!; sleep 0.5; done",
      "IsAfterReboot": false
    }
  ],
  "PostInstallationCmds": []
}
```

</details>

### Run Task

```sh
curl --request GET \
  --url http://0.0.0.0:5028/tasks/run/3
```

<details>
<summary>Sample Response <strong>Run Task</strong></summary>

```json
{
  "IsRunning": false,
  "RunningState": 0,
  "StageCMD": "",
  "StageProgress": 0,
  "StageSubProgress": -1,
  "StageSubProgressMsg": "",
  "StageCount": 6,
  "ErrorCode": 0,
  "ErrorMsg": ""
}
```

</details>

### Run Task With File Path

If you know the file path of the tar file for installation. You can use the API by adding the base64 encoded string to the end of the URL.

Sample URL

    http://10.5.178.253:5028/tasks/run/0/<base64_encoded_path>

Simple Request for <b>/tmp/test.tar.gz</b> file

```sh
curl --request GET \
  --url http://10.5.178.253:5028/tasks/run/0/L3RtcC90ZXN0LnRhci5neg==
```

### Get Task Status

```sh
curl --request GET \
  --url http://0.0.0.0:5028/tasks/status
```

<details>
<summary>Sample Response <strong>Get Task Status</strong></summary>

```json
{
  "IsRunning": false,
  "RunningState": 0,
  "StageCMD": "",
  "StageProgress": 6,
  "StageSubProgress": 100,
  "StageSubProgressMsg": " ieg",
  "StageCount": 6,
  "ErrorCode": 0,
  "ErrorMsg": "Task successfully finish"
}
```

</details>
