#!/bin/sh

dotnet publish -c Release -r ubuntu-arm64 -o build-arm64

# Get deb package template
wget https://gitlab.com/ozogulf/ci-files/raw/master/paketleme/DEBIAN_FILES.tar.gz
tar -xvf DEBIAN_FILES.tar.gz

# Get prepare
wget https://gitlab.com/ozogulf/ci-files/raw/master/paketleme/prepare-deb.sh
chmod +x prepare-deb.sh

# Get tusd
wget https://github.com/tus/tusd/releases/download/v1.0.2/tusd_linux_arm.tar.gz

# Create deb folder
./prepare-deb.sh -a updatemanager -u ieg -e iegursoy28@gmail.com -v 0.0.1

# Put app files
mkdir -p updatemanager_0.0.1/usr/local/bin/um
cp build-arm64/* updatemanager_0.0.1/usr/local/bin/um/

# Put tusd files
mkdir -p updatemanager_0.0.1/usr/local/bin/tusd
tar -xvf tusd_linux_arm.tar.gz -C updatemanager_0.0.1/usr/local/bin/tusd
mv updatemanager_0.0.1/usr/local/bin/tusd/tusd_linux_arm/tusd updatemanager_0.0.1/usr/local/bin/tusd/tusd && rm -rf updatemanager_0.0.1/usr/local/bin/tusd/tusd_linux_arm

# Put services
mkdir -p updatemanager_0.0.1/etc/systemd/system/
cp deb_services/*.service updatemanager_0.0.1/etc/systemd/system/

# Put post installation sh
cp deb_services/postinst updatemanager_0.0.1/DEBIAN/

# Create deb
dpkg-deb -b updatemanager_0.0.1

# Rename deb
cp updatemanager_0.0.1.deb updatemanager_arm64_0.0.1.deb