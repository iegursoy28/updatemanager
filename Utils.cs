using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Collections.Generic;

public class Utils
{
    public delegate void CMDStandartOutput(string s);

    public static string RunCMD(string cmd)
    {
        return RunCMD(null, cmd, null);
    }

    public static string RunCMD(string workDir, string cmd, CMDStandartOutput c)
    {
        if (cmd == null || cmd.Length <= 0)
            return "";

        if (workDir == null || workDir.Length <= 0)
            workDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        ProcessStartInfo startInfo = new ProcessStartInfo()
        {
            FileName = "/bin/bash",
            ArgumentList = { "-c", $"cd {workDir} && {cmd}" },
            UseShellExecute = false,
            RedirectStandardOutput = true,
            CreateNoWindow = true
        };
        Process proc = new Process() { StartInfo = startInfo, };
        proc.Start();

        string res = "";
        while (!proc.StandardOutput.EndOfStream)
        {
            string line = proc.StandardOutput.ReadLine();
            if (c != null) c(line);
            res += line + "\n";
        }

        if (!proc.HasExited)
            proc.WaitForExit(4000);

        return res;
    }

    public static TaskModel GetInstallationModel(DBManager db, TaskModel.InstallationType type, string full_file_path)
    {
        TaskModel task = null;
        switch (type)
        {
            case TaskModel.InstallationType.TAR:
                string defaultFileName = "installation.tar.gz";
                if (full_file_path != null && !string.IsNullOrEmpty(full_file_path))
                    defaultFileName = full_file_path;

                if (!db.Map.ContainsKey(TaskModel.InstallationType.TAR.ToString()) || !string.IsNullOrEmpty(full_file_path))
                {
                    var t = new TaskModel()
                    {
                        Name = $"installationtype_tar",
                        WorkDir = "/tmp/",
                        Type = TaskModel.InstallationType.TAR,
                        PreInstallationCmds = new TaskCMDModel[] {
                            new TaskCMDModel() {Cmd = "mkdir -m 777 -p ./setup"},
                            new TaskCMDModel() {Cmd = $"tar -xf {defaultFileName} -C ./setup"}
                        },
                        InstallationCmds = new TaskCMDModel[] {
                            new TaskCMDModel() {Cmd = "cd ./setup && chmod +x install.sh && ./install.sh"}
                        },
                        PostInstallationCmds = { }
                    };
                    db.Put(TaskModel.InstallationType.TAR.ToString(), t.ToString());
                    task = t;
                }

                string jtar = db.Map.GetValueOrDefault(TaskModel.InstallationType.TAR.ToString(), "");
                task = System.Text.Json.JsonSerializer.Deserialize<TaskModel>(jtar);

                break;
            case TaskModel.InstallationType.EXECUTABLE:
                if (!db.Map.ContainsKey(TaskModel.InstallationType.EXECUTABLE.ToString()))
                {
                    var t = new TaskModel()
                    {
                        Name = $"installationtype_executable",
                        WorkDir = "/",
                        Type = TaskModel.InstallationType.EXECUTABLE,
                        PreInstallationCmds = { },
                        InstallationCmds = { },
                        PostInstallationCmds = { }
                    };
                    db.Put(TaskModel.InstallationType.EXECUTABLE.ToString(), t.ToString());
                    task = t;
                }

                string jexec = db.Map.GetValueOrDefault(TaskModel.InstallationType.EXECUTABLE.ToString(), "");
                task = System.Text.Json.JsonSerializer.Deserialize<TaskModel>(jexec);

                break;
            case TaskModel.InstallationType.ZIP:
                if (!db.Map.ContainsKey(TaskModel.InstallationType.ZIP.ToString()))
                {
                    var t = new TaskModel()
                    {
                        Name = $"installationtype_zip",
                        WorkDir = "/",
                        Type = TaskModel.InstallationType.ZIP,
                        PreInstallationCmds = { },
                        InstallationCmds = { },
                        PostInstallationCmds = { }
                    };
                    db.Put(TaskModel.InstallationType.ZIP.ToString(), t.ToString());
                    task = t;
                }

                string jzip = db.Map.GetValueOrDefault(TaskModel.InstallationType.ZIP.ToString(), "");
                task = System.Text.Json.JsonSerializer.Deserialize<TaskModel>(jzip);

                break;
            case TaskModel.InstallationType.BASH:
                if (!db.Map.ContainsKey(TaskModel.InstallationType.BASH.ToString()))
                {
                    var t = new TaskModel()
                    {
                        Name = $"installationtype_bash",
                        WorkDir = "/",
                        Type = TaskModel.InstallationType.BASH,
                        PreInstallationCmds = { },
                        InstallationCmds = { },
                        PostInstallationCmds = { }
                    };
                    db.Put(TaskModel.InstallationType.BASH.ToString(), t.ToString());
                    task = t;
                }

                string jbash = db.Map.GetValueOrDefault(TaskModel.InstallationType.BASH.ToString(), "");
                task = System.Text.Json.JsonSerializer.Deserialize<TaskModel>(jbash);

                break;
        }

        return task;
    }

    public static void SetInstallationModel(DBManager db, TaskModel model)
    {
        if (db == null || model == null)
            return;

        switch (model.Type)
        {
            case TaskModel.InstallationType.TAR:
                model.Name = $"installationtype_tar";
                db.Put(TaskModel.InstallationType.TAR.ToString(), model.ToString());
                break;
            case TaskModel.InstallationType.EXECUTABLE:
                model.Name = $"installationtype_executable";
                db.Put(TaskModel.InstallationType.EXECUTABLE.ToString(), model.ToString());
                break;
            case TaskModel.InstallationType.ZIP:
                model.Name = $"installationtype_zip";
                db.Put(TaskModel.InstallationType.ZIP.ToString(), model.ToString());
                break;
            case TaskModel.InstallationType.BASH:
                model.Name = $"installationtype_bash";
                db.Put(TaskModel.InstallationType.BASH.ToString(), model.ToString());
                break;
        }
    }

    public static bool ParseSubProgress(string log, out int p, out string msg)
    {
        string tmp = log;
        msg = "";
        p = -1;

        tmp = tmp.Trim();
        if (tmp.Length <= 6)
            return false;
        if (!tmp.StartsWith("!==") && !tmp.EndsWith("==!"))
            return false;
        tmp = tmp.Substring(3, tmp.Length - 6);
        if (tmp.Contains(':'))
        {
            string[] a = tmp.Split(':');
            if (a.Length != 2)
                return false;

            msg = a[1];

            int o;
            if (!int.TryParse(a[0].Trim(), out o))
                return false;
            p = o;
        }
        else
        {
            int o;
            if (!int.TryParse(tmp.Trim(), out o))
                return false;
            p = o;
        }

        return true;
    }
}